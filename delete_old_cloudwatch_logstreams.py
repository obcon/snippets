import json
import boto3
import time

NOW = int(time.time() * 1000)
LOGS = boto3.client('logs')

def handle_log_streams(logGroupName, response):
    for logStream in response.get("logStreams",[]):
        logStreamName = logStream["logStreamName"]
        if "lastEventTimestamp" in logStream:
            hours = int((NOW - logStream["lastEventTimestamp"]) / 1000 / 60 / 60)
            if hours > 48:
                print("DELETE", hours, "hours old", logGroupName, logStreamName)
                LOGS.delete_log_stream(
                    logGroupName=logGroupName,
                    logStreamName=logStreamName
                )

def handle_log_groups(response):
    # print(json.dumps(response))
    for logGroup in response.get("logGroups",[]):
        logGroupName = logGroup["logGroupName"]
        response = LOGS.describe_log_streams(
            logGroupName=logGroupName,
            orderBy='LastEventTime',
            descending=False
        )
        handle_log_streams(logGroupName, response)
        while 'nextToken' in response:
            response = LOGS.describe_log_streams(logGroupName=logGroupName, nextToken=response['nextToken'])
            handle_log_streams(logGroupName, response)

def lambda_handler(event, context):
    response = LOGS.describe_log_groups()
    handle_log_groups(response)
    while 'nextToken' in response:
        response = LOGS.describe_log_groups(nextToken=response['nextToken'])
        handle_log_groups(response)

