import boto3
import json

ACCOUNT_ID = "236580801904"

ROLE_ARN = "arn:aws:iam::{}:role/iot-access".format(ACCOUNT_ID)

def aws_session(role_arn=None, session_name='my_session'):
    if role_arn:
        client = boto3.client('sts')

        device_topic = "cactus/{}/{}/{}/{}".format("1234567890123", "uuid-project-1234", "uuid-product-1234", "device-serial-1234")
        resource_prefix = "arn:aws:iot:{}:{}".format("eu-central-1", ACCOUNT_ID)

        policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "iot:Connect",
                        "iot:Receive"
                    ],
                    "Resource": [
                        "*"
                    ]
                },
                {
                    "Effect": "Allow",
                    "Action": [
                        "iot:Subscribe"
                    ],
                    "Resource": [
                        resource_prefix + ":topicfilter/" + device_topic + "/cloud/+/+/+"
                    ]
                }
            ]
        }
        response = client.assume_role(RoleArn=role_arn, RoleSessionName=session_name, DurationSeconds=900, Policy=json.dumps(policy))
        # print(response)
        session = boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken'])
        return session, policy
    else:
        return boto3.Session()

def lambda_handler(event, context):
    session_assumed, policy = aws_session(role_arn=ROLE_ARN, session_name='marco.obermeyer@obcon.de')
    print(session_assumed.client('sts').get_caller_identity()["Arn"])
    print(json.dumps(policy, indent=2))
