import boto3
import arrow
import os

from botocore.signers import CloudFrontSigner
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding

SSM = boto3.client('ssm')

def _rsa_signer(message):
        pk = os.environ["cloudfront_key_pk"]

        private_key = serialization.load_pem_private_key(
            pk.encode(),
            password=None,
            backend=default_backend()
        )
        signer = private_key.signer(padding.PKCS1v15(), hashes.SHA1())
        signer.update(message)
        return signer.finalize()

def sign():
    parameters_map = {}

    parameters_list = SSM.get_parameters(
        Names=[
            '/global/cloudfront_key_pk',
            '/global/cloudfront_key_id'
        ],
        WithDecryption=True
    ).get('Parameters', [])
    
    for parameter in parameters_list:
        parameters_map[parameter.get('Name')] = parameter.get('Value')

    print(parameters_map)

    os.environ["cloudfront_key_pk"] = parameters_map["/global/cloudfront_key_pk"]

    cloudfront_key_id = parameters_map["/global/cloudfront_key_id"]

    cloudfront_signer = CloudFrontSigner(cloudfront_key_id, _rsa_signer)
    expire_date = arrow.utcnow().shift(minutes=+500)
    signed_url = cloudfront_signer.generate_presigned_url(
        'https://{}/{}'.format("1456583191702.assets.ed8f0.cactus.siemens.cloud", "test.html"),
        date_less_than=expire_date.datetime
    )
    print(signed_url)

if __name__ == "__main__":
    sign()