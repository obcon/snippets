import json
import boto3

dynamodb = boto3.resource('dynamodb')
old = dynamodb.Table('ed8f0_global_registry')
new = dynamodb.Table('ed8f0_global_regstr')

def lambda_handler(event, context):

    response = old.scan()
    data = response['Items']
    
    while 'LastEvaluatedKey' in response:
        response = old.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
        data.extend(response['Items'])
        
    print(len(data))
    for item in data:
        new.put_item(Item=item)
