import boto3

IoT = boto3.client('iot')
IoTData = boto3.client('iot-data')

list_things = IoT.get_paginator('list_things')

def lambda_handler(event, context):
    page_iterator = list_things.paginate()
    
    for page in page_iterator:
        for thing in page['things']:
            thingName = thing['thingName']
            try:
                IoTData.delete_thing_shadow(
                   thingName=thingName
                )
                print(f"{thingName} deleted")
            except Exception as e:
                pass