import json
import boto3
import time

LOGS = boto3.client('logs')

def handle_log_groups(response):
    # print(json.dumps(response))
    for logGroup in response.get("logGroups",[]):
        logGroupName = logGroup["logGroupName"]
        print(f"set retention for {logGroupName}")
        response = LOGS.put_retention_policy(
            logGroupName=logGroupName,
            retentionInDays=3
        )

def lambda_handler(event, context):
    response = LOGS.describe_log_groups()
    handle_log_groups(response)
    while 'nextToken' in response:
        response = LOGS.describe_log_groups(nextToken=response['nextToken'])
        handle_log_groups(response)

